package io.pragra.fullstack.demo.dto;

import io.pragra.fullstack.demo.model.Category;

import java.util.UUID;

public class ProductDTO {

    private UUID skuId;

    private Category category;

    private double price;

    private String description;

    public ProductDTO() {
    }

    public ProductDTO(UUID skuId, Category category, double price, String description) {
        this.skuId = skuId;
        this.category = category;
        this.price = price;
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UUID getskuId() {
        return skuId;
    }

    public void setskuId(UUID skuId) {
        this.skuId = skuId;
    }
}
