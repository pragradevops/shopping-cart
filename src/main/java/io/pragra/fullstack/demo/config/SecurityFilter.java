package io.pragra.fullstack.demo.config;


import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;

@Component
public class SecurityFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("DO Filter");
        System.out.println(servletRequest.getParameterNames());
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("Filter INit");
    }

    @Override
    public void destroy() {

    }
}
