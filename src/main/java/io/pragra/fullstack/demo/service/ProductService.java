package io.pragra.fullstack.demo.service;

import io.pragra.fullstack.demo.dto.ProductDTO;
import io.pragra.fullstack.demo.mapper.ProductMapper;
import io.pragra.fullstack.demo.model.Category;
import io.pragra.fullstack.demo.model.Product;
import io.pragra.fullstack.demo.repository.ProductRepo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProductService {

    private ProductRepo repo;

    private ProductMapper mapper;

    public ProductService(ProductRepo repo, ProductMapper mapper) {
        this.repo = repo;
        this.mapper = mapper;
    }

    public List<ProductDTO> getProducts() {
        List<Product> products = repo.findAll();
        return  products.stream().map(s->{
            ProductDTO dto = new ProductDTO();
            mapper.mapProductToProductDto(s,dto);
            return dto;
        }).collect(Collectors.toList());
    }

    public ProductDTO createProduct(ProductDTO productDTO) {
        Product product = new Product();
        mapper.mapDtoToProduct(productDTO,product);
        if(null==product.getSkuId()){
            product.setSkuId(UUID.randomUUID());
        }
        mapper.mapProductToProductDto(repo.save(product),productDTO);
        return productDTO;
    }

/*
    public ProductDTO updateProduct(ProductDTO productDTO) {
        Product product = new Product();
        mapper.mapDtoToProduct(productDTO,product);
        if(null==product.getSkuId()){
            product.setSkuId(UUID.randomUUID());
        }
        mapper.mapProductToProductDto(repo.save(product),productDTO);
        return productDTO;
    }
*/

    public ProductDTO findProduct(UUID skuId) {
        Product product = repo.findById(skuId).get();
            ProductDTO dto = new ProductDTO();
            mapper.mapProductToProductDto(product,dto);
            return dto;
    }

    public void deleteProduct(UUID skuId) {
        //Optional<Product> product = repo.findById(skuId);
        repo.deleteById(skuId);
    }
}
