package io.pragra.fullstack.demo.mapper;

import io.pragra.fullstack.demo.dto.CategoryDTO;
import io.pragra.fullstack.demo.model.Category;
import org.springframework.stereotype.Component;

@Component
public class CategoryMapper {

    public void mapCategoryToCategoryDto(Category src, CategoryDTO dest){
        dest.setId(src.getId());
        dest.setName(src.getName());
    }

    public void mapDtoToCategory(CategoryDTO src, Category dest){
        dest.setId(src.getId());
        dest.setName(src.getName());
    }
}
