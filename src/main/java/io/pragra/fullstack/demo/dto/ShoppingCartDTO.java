package io.pragra.fullstack.demo.dto;

import io.pragra.fullstack.demo.model.Product;

import java.util.List;

public class ShoppingCartDTO {

    private List<Product> items;

    private double total ;

    public List<Product> getItems() {
        return items;
    }

    public void setItems(List<Product> items) {
        this.items = items;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
}
