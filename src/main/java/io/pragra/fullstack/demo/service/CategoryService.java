package io.pragra.fullstack.demo.service;

import io.pragra.fullstack.demo.dto.CategoryDTO;
import io.pragra.fullstack.demo.mapper.CategoryMapper;
import io.pragra.fullstack.demo.model.Category;
import io.pragra.fullstack.demo.repository.CategoryRepo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryService {

    private CategoryMapper categoryMapper;
    private CategoryRepo categoryRepo;

    public CategoryService(CategoryMapper categoryMapper, CategoryRepo categoryRepo) {
        this.categoryMapper = categoryMapper;
        this.categoryRepo = categoryRepo;
    }

    public List<CategoryDTO> getCategories(){
        List<Category> categories = categoryRepo.findAll();
        return categories.stream().map(m -> {
            CategoryDTO categoryDTO = new CategoryDTO();
            categoryMapper.mapCategoryToCategoryDto(m, categoryDTO);
            return categoryDTO;
        }).collect(Collectors.toList());
        }

    public CategoryDTO addCategory(CategoryDTO categoryDTO){
        Category category = new Category();
        categoryMapper.mapDtoToCategory(categoryDTO, category);
        categoryMapper.mapCategoryToCategoryDto(categoryRepo.save(category), categoryDTO);
        return categoryDTO;
    }

}
