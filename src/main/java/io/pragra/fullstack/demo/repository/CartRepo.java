package io.pragra.fullstack.demo.repository;

import io.pragra.fullstack.demo.model.ShoppingCart;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepo extends JpaRepository<ShoppingCart,String> {
}
