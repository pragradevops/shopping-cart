package io.pragra.fullstack.demo.dto;

import io.pragra.fullstack.demo.model.Product;

import java.util.List;

public class CatelogueDTO {

    List<Product> products;


    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
