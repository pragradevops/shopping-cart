package io.pragra.fullstack.demo.controllers;

import io.pragra.fullstack.demo.service.ProductService;
import io.pragra.fullstack.demo.dto.ProductDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api")
public class ProductController {

    private ProductService service;

    public ProductController(ProductService service) {
        this.service = service;
    }



    @PostMapping("/product")
    public ResponseEntity<ProductDTO> createProduct(@RequestBody ProductDTO dto ){
       ProductDTO productDTO = service.createProduct(dto);
       if(productDTO!=null){
           return ResponseEntity.status(HttpStatus.CREATED).body(productDTO);
       }
       return ResponseEntity.badRequest().build();
    }

    @PutMapping("/product")
    public ResponseEntity<ProductDTO> updateProduct(@RequestBody ProductDTO dto ){
        ProductDTO productDTO = service.createProduct(dto);
        if(productDTO!=null){
            return ResponseEntity.status(HttpStatus.CREATED).body(productDTO);
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/product")
    public ResponseEntity<List<ProductDTO>> getAll(){
        return ResponseEntity.status(HttpStatus.OK).body(service.getProducts());
    }

    @GetMapping("/product/{skuId}")
    public ResponseEntity<ProductDTO> getById(@PathVariable UUID skuId){
        ProductDTO productDTO = service.findProduct(skuId);
        if(productDTO != null) {
            return ResponseEntity.status(HttpStatus.OK).body(productDTO);
        }
        return ResponseEntity.badRequest().build();
    }

    @DeleteMapping("/product/{skuId}")
    public ResponseEntity<String> deleteProduct(@PathVariable UUID skuId){
        ResponseEntity<ProductDTO> productDTO = getById(skuId);
        if(productDTO != null){
            service.deleteProduct(skuId);
            return ResponseEntity.status(HttpStatus.OK).body("{\"skuId deleted\":\""+skuId+"\"}");
        }
        else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"skuId doesn't exist\":\"skuId\"}");
        }
    }


}
