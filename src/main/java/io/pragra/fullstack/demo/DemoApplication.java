package io.pragra.fullstack.demo;

import io.pragra.fullstack.demo.model.Catelogue;
import io.pragra.fullstack.demo.repository.CateLogueRepo;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DemoApplication {

    private CateLogueRepo repo;

    public DemoApplication(CateLogueRepo repo) {
        this.repo = repo;
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
    ApplicationRunner runner(){
        return args -> {
            repo.save(new Catelogue());
        };
    }
}
