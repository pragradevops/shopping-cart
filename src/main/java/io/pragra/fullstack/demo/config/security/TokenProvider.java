//package io.pragra.fullstack.demo.config.security;
//
//import com.sun.org.apache.xml.internal.security.algorithms.SignatureAlgorithm;
//import org.springframework.beans.factory.annotation.Autowired;


//import javax.crypto.spec.SecretKeySpec;
//import javax.xml.bind.DatatypeConverter;
//import java.security.Key;
//
//public class TokenProvider {
//    private static final String secretKey= "4C8kum4LxyKWYLM78sKdXrzbBjDCFyfX";
//
//    public String createToken(String subject, long ttlMillis) {
//        if (ttlMillis <= 0) {
//            throw new RuntimeException("Expiry time must be greater than Zero :["+ttlMillis+"] ");
//        }
//        // The JWT signature algorithm we will be using to sign the token
//        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
//        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(secretKey);
//        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
//         Jwts.builder()
//                .setSubject(subject)
//                .signWith(signatureAlgorithm, signingKey);
//        long nowMillis = System.currentTimeMillis();
//        builder.setExpiration(new Date(nowMillis + ttlMillis));
//        return builder.compact();
//    }
//
//}
