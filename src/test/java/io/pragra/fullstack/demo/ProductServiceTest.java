package io.pragra.fullstack.demo;

import io.pragra.fullstack.demo.dto.ProductDTO;
import io.pragra.fullstack.demo.model.Category;
import io.pragra.fullstack.demo.service.ProductService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class ProductServiceTest {

    @Autowired
    protected ProductService service;

    @Before
    public void setUp() throws Exception {
        service.createProduct(new ProductDTO(UUID.randomUUID(),new Category("abc"), 200.0, "new Product"));
        service.createProduct(new ProductDTO(UUID.randomUUID(),new Category("abc"), 100.0, "new Product"));
        service.createProduct(new ProductDTO(UUID.randomUUID(),new Category("abc"), 200.0, "new Product"));
        service.createProduct(new ProductDTO(UUID.randomUUID(),new Category("abc"), 300.0, "new Product"));
        service.createProduct(new ProductDTO(UUID.randomUUID(),new Category("abc"), 400.0, "new Product"));
    }

    @Test
    public void getAllTest() {
        List<ProductDTO> products = service.getProducts();
        Assert.assertEquals(products.size(), 5);
    }

    @Test
    public void  getOneTest(){
        //
    }
}
