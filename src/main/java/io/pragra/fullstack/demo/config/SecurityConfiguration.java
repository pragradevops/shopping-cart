//package io.pragra.fullstack.demo.config;
//
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.HttpMethod;
//import org.springframework.security.config.annotation.web.builders.WebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.config.annotation.web.servlet.configuration.WebMvcSecurityConfiguration;
//
//@Configuration
//public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
//
//    @Override
//    public void configure(WebSecurity web) throws Exception {
//        web.ignoring()
//                .antMatchers(HttpMethod.OPTIONS, "/**")
//                .antMatchers("/api/product/**")
//                .antMatchers("/h2-console/**");
//    }
//
////
////    @Override
////    protected void configure(HttpSecurity http) throws Exception {
////        http
////                .addFilterBefore(corsFilter, UsernamePasswordAuthenticationFilter.class)
////                .exceptionHandling()
////                .authenticationEntryPoint(problemSupport)
////                .accessDeniedHandler(problemSupport)
////                .and()
////                .csrf()
////                .disable()
////                .headers()
////                .frameOptions()
////                .disable()
////                .and()
////                .sessionManagement()
////                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
////                .and()
////                .authorizeRequests()
////                .antMatchers("/api/register").permitAll()
////                .antMatchers("/api/register-email").permitAll()
////                .antMatchers("/api/activate").permitAll()
////                .antMatchers("/api/activate-account").permitAll()
////                .antMatchers("/api/authenticate").permitAll()
////                .antMatchers("/api/account/reset-password/init").permitAll()
////                .antMatchers("/api/account/reset-password/finish").permitAll()
////
////                .antMatchers("/api/account/**").authenticated()
////                .antMatchers("/api/profile-info").permitAll()
////
////                .antMatchers("/api/**").authenticated()
////                .antMatchers("/management/health").permitAll()
////                .antMatchers("/management/info").permitAll()
////                .antMatchers("/management/**").hasAuthority(AuthoritiesConstants.ADMIN)
////                .antMatchers("/v2/api-docs/**").permitAll()
////                .antMatchers("/swagger-resources/configuration/ui").permitAll()
////                .antMatchers("/swagger-ui/index.html").hasAuthority(AuthoritiesConstants.ADMIN)
////                .and()
////                .apply(securityConfigurerAdapter());
////
////    }
//}
