package io.pragra.fullstack.demo.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "CatelogueDTO")
public class Catelogue {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;



    @OneToMany
    List<Product> products;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
