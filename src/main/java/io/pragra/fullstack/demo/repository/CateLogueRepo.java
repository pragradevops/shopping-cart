package io.pragra.fullstack.demo.repository;


import io.pragra.fullstack.demo.model.Catelogue;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CateLogueRepo extends JpaRepository<Catelogue, Integer> {
}
