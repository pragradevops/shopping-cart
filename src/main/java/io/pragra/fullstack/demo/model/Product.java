package io.pragra.fullstack.demo.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;


@Entity
@Table(name = "PRODUCTS")
public class Product {

    @Id
    private UUID skuId;


    @OneToOne(cascade = CascadeType.ALL)
    private Category category;

    private double price;

    private String description;

    private double actucalCost;

    private double maxDiscount;

    public UUID getSkuId() {
        return skuId;
    }

    public void setSkuId(UUID skuId) {
        this.skuId = skuId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getActucalCost() {
        return actucalCost;
    }

    public void setActucalCost(double actucalCost) {
        this.actucalCost = actucalCost;
    }

    public double getMaxDiscount() {
        return maxDiscount;
    }

    public void setMaxDiscount(double maxDiscount) {
        this.maxDiscount = maxDiscount;
    }
}
