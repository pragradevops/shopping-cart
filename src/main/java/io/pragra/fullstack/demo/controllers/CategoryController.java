package io.pragra.fullstack.demo.controllers;

import io.pragra.fullstack.demo.service.CategoryService;
import io.pragra.fullstack.demo.dto.CategoryDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CategoryController {

    private CategoryService categoryService;


    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/category")
    ResponseEntity<List<CategoryDTO>> findAll(){
        return ResponseEntity.status(HttpStatus.OK).body(categoryService.getCategories());
    }

    @PostMapping("/category")
    ResponseEntity<CategoryDTO> addCategory(@RequestBody CategoryDTO DTO){
        CategoryDTO categoryDTO = categoryService.addCategory(DTO);
        if(categoryDTO!=null){
            return ResponseEntity.status(HttpStatus.CREATED).body(categoryDTO);
        }
        return ResponseEntity.badRequest().build();

    }
}
