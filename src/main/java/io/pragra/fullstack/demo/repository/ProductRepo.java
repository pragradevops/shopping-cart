package io.pragra.fullstack.demo.repository;

import io.pragra.fullstack.demo.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface ProductRepo extends JpaRepository<Product,UUID> {
    Optional<Product> findById(UUID skuId);

    void deleteById(UUID skuId);
}
