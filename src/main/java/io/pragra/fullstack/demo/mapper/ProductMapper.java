package io.pragra.fullstack.demo.mapper;

import io.pragra.fullstack.demo.dto.ProductDTO;
import io.pragra.fullstack.demo.model.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper {


    public void mapProductToProductDto(Product src, ProductDTO dest){
        dest.setskuId(src.getSkuId());
        dest.setCategory(src.getCategory());
        dest.setPrice(src.getPrice());
        dest.setDescription(src.getDescription());

    }

    public void mapDtoToProduct(ProductDTO src,Product dest){

        dest.setSkuId(src.getskuId());
        dest.setCategory(src.getCategory());
        dest.setPrice(src.getPrice());
        dest.setDescription(src.getDescription());
        dest.setActucalCost(100);
        dest.setMaxDiscount(40);
    }
}
